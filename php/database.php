<?php

function open_db() {
	//check if we're in dev environment
	if (strcasecmp(substr($_SERVER['DOCUMENT_ROOT'], 0, 4), '/var' ) == 0) {
		$homePath = '/var/www/kathyapplebaum';
		require_once '/var/www/db_connect.php';
	}

	else {
		$homePath = '/home/kathyapp/www';
		require_once '/home/kathyapp/db_connect.php';
	}

	try {
		$connectionData = get_db_info();
		$db = new PDO($connectionData['dsn'], $connectionData['username'],
						$connectionData['password']);
	}
	catch(PDOException $e) {
		$error_message = $e->getMessage();
		include($homePath.'/mlb/error/database_error.php');
		sendErrorEmail($error_message);
		exit();
	}
	return $db;
}

/**
 * Get game data from database or web
 *
 * @param DateTime $date date for which we want data
 * @return associative array of data
 */
function getGameData($date) {
	$gameData = array();

	// only try to get data if we have a date-like string
	if(strtotime($date) !== false)
	{
		try {
			$db = $open_db();
			//TODO add functionality
			// check if $date is in game database
			// check if $date is in no_game database
			// scrape from web
		}
		catch(PDOException $e) {
			$gameData = null;
		}
	}

	return $gameData;
}

function test_con(){
	$db = open_db();
	$strQuery = "SELECT *
		FROM team";
	$statement = $db->prepare($strQuery);
	$statement->execute();
	$result = $statement->fetchAll();
	$statement->closeCursor();
	return $result;
}

?>
