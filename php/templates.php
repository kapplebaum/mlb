<?php

/*
 * File: templates.php
 * Functions to construct common headers, etc.
 */

function show_header($pageTitle, $pageDescription) {
	echo '<meta name="description" content="' . $pageDescription . '">' . "\n";
	echo "    " . '<link href="css/reset.css" rel="stylesheet" type="text/css">' . "\n";
	echo "    " . '<link href="css/960.css" rel="stylesheet" type="text/css">' . "\n";
	echo "    " . '<link href="css/text.css" rel="stylesheet" type="text/css">' . "\n";
	echo "    " . '<link href="css/kathy.css" rel="stylesheet" type="text/css">' . "\n";
	echo "    " . '<title>' . $pageTitle . '</title>' . "\n";
}

/*
 * Standard navigation for ease of maintenance
 */

function show_navbar($pageName) {
	echo '<div class="grid_12">' . "\n";
	echo '<h1>Kathy Applebaum</h1>' . "\n";
	echo '<hr>' . "\n";

// is the page the home page?
	if(strcasecmp($pageName, 'home') == 0)
	{
		echo '<span class="grid_1 grey_text bold_text"><strong>Home</strong></span>' . "\n";
	}
	else
	{
		echo '<span class="grid_1"><a href="http://www.kathyapplebaum.com">Home</a></span>' . "\n";
	}

// is it the blog page? (should not happen for now...)
	if(strcasecmp($pageName, 'blog') == 0)
	{
		echo '<span class="grid_1 grey_text bold_text"><strong>Blog</strong></span>' . "\n";
	}
	else
	{
		echo '<span class="grid_1"><a href="/blog">Blog</a></span>' . "\n";
	}


// is it the education page
	if(strcasecmp($pageName, 'education') == 0)
	{
		echo '<span class="grid_1 grey_text bold_text"><strong>Education</strong></span>' . "\n";
	}
	else
	{
		echo '<span class="grid_1"><a href="/bio/education.html">Education</a></span>' . "\n";
	}

	echo '<div class="clear"></div>' . "\n";
	echo '<hr class="top_space">' . "\n";
	echo '</div>' . "\n";
}

// TODO: don't let user link to bad date. Use isGoodQueryFormat?
function show_prior_date_link($date) {
	$priorDate = clone $date;
	$priorDate->sub(new DateInterval('P1D'));
	echo '<a href="index.html?' . $priorDate->format('Y-m-d') . '">';
	echo $priorDate->format('F j, Y');
	echo '</a>';
}

function show_date_link($date) {
	echo '<strong>' . $date->format('F j, Y') . '</strong>';
}

function show_next_date_link($date) {
	$today = new DateTime();
	$nextDay = clone $date;
	$nextDay->add(new DateInterval('P1D'));
	if(strcmp($today->format('Y-m-d'), $nextDay->format('Y-m-d')) > 0)
	{
		echo '<a href="index.html?' . $nextDay->format('Y-m-d') . '">';
		echo $nextDay->format('F j, Y');
		echo '</a>';
	}
}

/**
 * Checks if $inStr is a well-formatted query string the program can handle.
 *
 * Current rejects dates prior to 2010 because MLBs directory incomplete
 * before then.
 *
 * @todo update web scraping to handle mlb incompleteness
 *
 * @param string $inStr
 * @return boolean True if good query string
 */
function isGoodQueryFormat($inStr) {
	$goodQuery = true;
	$today = new DateTime;
	if($today->format('Y-m-d') <= $inStr ||
					'2010-01-01' > $inStr ||
					!strtotime ($inStr))
		$goodQuery = false;

	return $goodQuery;
}

?>
